# Gelombang 2324
 
 Pertemuan | Learning Outcome | Materi & Asesmen
---|---|---
1-2 | <ol><li>Mampu Mengenali konsep Muatan </li><li> Memahami konsep interaksi muatan dan jenisnya </li><li>Mampu memahami perhitungan menggunakan hukum coloumb</li><li>Mampu Menidentifikasi dan menjelaskan proses lanjutan dari perhitungan hukum coulumb menggunakan analisis vektor|<ol><li>[Lectures notes : Osilasi Harmonis 1](https://drive.google.com/file/d/1cHdxohS4ujBW3skADE5tYQI-Z_Nyf_Ed/view?usp=share_link) </li><li>[Osilasi Harmonis Part 2](https://drive.google.com/file/d/1cK51yGg0g6-KoI5cZbhD0CcwOvY3CYAM/view?usp=share_link)
3-4 | <ol><li>Mengenal konsep Osilasi Teredam</li><li> Mampu memahami perhitungan dan persamaan gelombang untuk osilasi teredam</li><li> Memahami persamaan osilasi teredam pada rangakaian RLC </li><li> Memahami Aplikasi dan fungsi Osilasi teredam|<li><il> [Lectures note : Osilasi Teredam](https://drive.google.com/file/d/1cGNfzOuzFI8ALhUSf5tr-yLFx72Vn0pi/view?usp=drivesdk) update !!
5 |<ol><li>Mahasiswa mampu memberikan solusi pada permasalahan fisika</li><li> Mampu memahami perhitungan dan persamaan gelombang untuk osilasi teredam, pendulum dan osilasi sederhana </li><li> Memahami persamaan osilasi teredam pada rangakaian RLC </li><li> Memahami Aplikasi dan fungsi Osilasi teredam| <ol><li>[Extra notes Damped Ocillation LRC Circuit](https://drive.google.com/file/d/1dBVx9BYjaS6ySTGnTxfXVcuKeJI-anOF/view?usp=sharing) </li><li>[Exercise Class](https://drive.google.com/file/d/1dDYZPueyaQHFiIgC7BK0Sc1DXNZt2P_Z/view?usp=sharing) </li><li> [Promblem Set 1-2](https://drive.google.com/file/d/1JI8uZQ_5STu3Ri1voa8MXOhI3d5706KT/view?usp=sharing) UPDATE (for 28 March 2024 Please check and give the sample answer)
6 |QUIZ </li><li>
7-8 |<ol><li>Mahasiswa mampu memberikan solusi pada permasalahan fisika</li><li> Mampu memahami perhitungan dan persamaan gelombang untuk osilasi terpaksa, </li><li> Memahami persamaan osilasi terpaksa dan perilakunya pada rangakaian RLC </li><li> Memahami Aplikasi dan fungsi Osilasi terpaksa pada sirkuit lisstrik RLC| <ol><li>[Osilasi Terpaksa](https://drive.google.com/file/d/1db91Kx7JnlVG9DVJqSqq8w72HSeIS5Y0/view?usp=share_link) </li><li> [Osilasi Terkopel/tergandeng part 1](https://drive.google.com/file/d/1dcTK0wNPlT8aQX4LzcWl_j8S75NIetug/view?usp=sharing) UPDATE (for 21 Apeil 2024)
9 |<ol><li>Mahasiswa mampu memberikan solusi pada permasalahan fisika</li><li> Mampu memahami perhitungan dan persamaan gelombang berjalan  </li><li> Memahami persamaan Gelombang Harmonis </li><li> Memahami Aplikasi dan fungsi Gelombang Harmonis| <ol><li>[Persamaan Gelombang Berjalan](https://youtu.be/O0FU261noAU?si=z3mf6GKPoOwNWP8a) </li><li> [Persamaan Osilasi Harmonis](https://youtu.be/YvBXn_FAjYc?si=cg_jGZcRQLOaUmcN) </li><li> [Arah dan Kecepatan Gelombang](https://youtu.be/JIVb1OyidBE?si=p-MaBAE3cgW8Tjb8) </li><li>  Tugas : Resume materi dan sebutkan beberapa aplikasi dan deskripsi tentang gelombang berjalan dan harmonis penerapan dibidang industri dan kesehatan. Dikumpulkan tanggal 2 Mei 2024 UPDATE (for 25 April 2024) 



## Tools

[phyphox](https://phyphox.org)

## Referensi




## Tools

## Referensi



